package com.edgardribeiro.cursomc;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.edgardribeiro.cursomc.domain.Categoria;
import com.edgardribeiro.cursomc.domain.Cidade;
import com.edgardribeiro.cursomc.domain.Cliente;
import com.edgardribeiro.cursomc.domain.Endereco;
import com.edgardribeiro.cursomc.domain.Estado;
import com.edgardribeiro.cursomc.domain.ItemPedido;
import com.edgardribeiro.cursomc.domain.Pagamento;
import com.edgardribeiro.cursomc.domain.PagamentoComBoleto;
import com.edgardribeiro.cursomc.domain.PagamentoComCartao;
import com.edgardribeiro.cursomc.domain.Pedido;
import com.edgardribeiro.cursomc.domain.Produto;
import com.edgardribeiro.cursomc.domain.enums.EstadoPagamento;
import com.edgardribeiro.cursomc.domain.enums.TipoCliente;
import com.edgardribeiro.cursomc.repositories.CategoriaRepository;
import com.edgardribeiro.cursomc.repositories.CidadeRepository;
import com.edgardribeiro.cursomc.repositories.ClienteRepository;
import com.edgardribeiro.cursomc.repositories.EnderecoRepository;
import com.edgardribeiro.cursomc.repositories.EstadoRepository;
import com.edgardribeiro.cursomc.repositories.ItemPedidoRepository;
import com.edgardribeiro.cursomc.repositories.PagamentoRepository;
import com.edgardribeiro.cursomc.repositories.PedidoRepository;
import com.edgardribeiro.cursomc.repositories.ProdutoRepository;

@SpringBootApplication
public class CursomcApplication implements CommandLineRunner{
	
	@Autowired
	private CategoriaRepository categoriaRepo;
	@Autowired
	private ProdutoRepository produtoRepo;
	@Autowired
	private CidadeRepository cidadeRepo;
	@Autowired
	private EstadoRepository estadoRepo;
	@Autowired
	private ClienteRepository clienteRepo;
	@Autowired
	private EnderecoRepository enderecoRepo;
	@Autowired
	private PagamentoRepository pagamentoRepo;
	@Autowired
	private PedidoRepository pedidoRepo;
	@Autowired
	private ItemPedidoRepository itemPedidoRepo;

	public static void main(String[] args) {
		SpringApplication.run(CursomcApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Categoria cat1 = new Categoria(null, "Informática");
		Categoria cat2 = new Categoria(null, "Escritório");
		
		Produto p1 = new Produto(null, "Computador", 2000.00);
		Produto p2 = new Produto(null, "Impressora", 800.00);
		Produto p3 = new Produto(null, "Mouse", 80.00);
		
		cat1.getProdutos().addAll(Arrays.asList(p1, p2, p3));
		cat2.getProdutos().addAll(Arrays.asList(p2));
		
		p1.getCategorias().addAll(Arrays.asList(cat1));
		p2.getCategorias().addAll(Arrays.asList(cat1, cat2));
		p3.getCategorias().addAll(Arrays.asList(cat1));		
		
		categoriaRepo.saveAll(Arrays.asList(cat1, cat2));
		produtoRepo.saveAll(Arrays.asList(p1, p2, p3));
		
		Estado est1 = new Estado(null, "Goiás");
		Estado est2 = new Estado(null, "São Paulo");
		
		Cidade c1 = new Cidade(null, "Valparaíso de Goiás", est1);
		Cidade c2 = new Cidade(null, "São Paulo", est2);
		Cidade c3 = new Cidade(null, "Campinas", est2);
		
		est1.getCidades().addAll(Arrays.asList(c1));
		est2.getCidades().addAll(Arrays.asList(c2, c3));
		
		estadoRepo.saveAll(Arrays.asList(est1,est2));
		cidadeRepo.saveAll(Arrays.asList(c1, c2, c3));
		
		Cliente cli1 = new Cliente(null, "Sophie Avelina Monção Ribeiro", "sophieavelinamribeiro@gmail.com", "07252703142", TipoCliente.PESSOAFISICA);
		
		cli1.getTelefones().addAll(Arrays.asList("(61)982677175", "(61)993376116"));
		
		Endereco e1 = new Endereco(null, "Rua 8", "Qd. 34", "Lote 1 à 46 COnd. Bello Solare Bl. I apto 108", "Parque Esplanada 2", "72878060", cli1, c1);
		Endereco e2 = new Endereco(null, "Avenida Matos", "105", "Sala 800", "Centro", "38777012", cli1, c2);
		
		cli1.getEnderecos().addAll(Arrays.asList(e1, e2));
		
		clienteRepo.saveAll(Arrays.asList(cli1));
		enderecoRepo.saveAll(Arrays.asList(e1, e2));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		Pedido ped1 = new Pedido(null, sdf.parse("30/09/2017 10:32"), cli1, e1);
		Pedido ped2 = new Pedido(null, sdf.parse("10/10/2017 19:35"), cli1, e2);
		
		Pagamento pgto1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
		ped1.setPagamento(pgto1);
		
		Pagamento pgto2 = new PagamentoComBoleto(null, EstadoPagamento.PENDENTE, ped2, sdf.parse("20/10/2017 00:00"), null);
		ped2.setPagamento(pgto2);
		
		cli1.getPedidos().addAll(Arrays.asList(ped1, ped2));
		
		pagamentoRepo.saveAll(Arrays.asList(pgto1,pgto2));
		pedidoRepo.saveAll(Arrays.asList(ped1, ped2));
		
		ItemPedido ip1 = new ItemPedido(ped1, p1, 200.00, 1, 2000.00);
		ItemPedido ip2 = new ItemPedido(ped1, p3, 0.00, 2, 80.00);
		ItemPedido ip3 = new ItemPedido(ped2, p2, 100.00, 1, 800.00);
		
		ped1.getItens().addAll(Arrays.asList(ip1,ip2));
		ped2.getItens().addAll(Arrays.asList(ip3));
		
		p1.getItens().addAll(Arrays.asList(ip1));
		p2.getItens().addAll(Arrays.asList(ip3));
		p3.getItens().addAll(Arrays.asList(ip2));
		
		itemPedidoRepo.saveAll(Arrays.asList(ip1,ip2,ip3));
	}

}
