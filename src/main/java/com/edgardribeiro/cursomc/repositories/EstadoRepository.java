package com.edgardribeiro.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edgardribeiro.cursomc.domain.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Integer>{

}
